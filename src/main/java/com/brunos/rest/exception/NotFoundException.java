package com.brunos.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_GATEWAY)
public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {

        super(message);
    }
}