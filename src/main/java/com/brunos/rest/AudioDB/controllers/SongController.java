package com.brunos.rest.AudioDB.controllers;

import com.brunos.rest.AudioDB.model.Song;
import com.brunos.rest.AudioDB.services.SongService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/songs")
public class SongController {

    @Autowired
    private SongService Service;

    // GET /songs
    @GetMapping("")
    public List<Song> getAllSongs() {
        return Service.getAll();
    }

    // GET /songs/{id}
    @GetMapping("/{id}")
    public Song getSong(@PathVariable("id") int id) throws NotFoundException {
        Song Song = Service.getSongByID(id);
        if (Song == null) {
            throw new NotFoundException("id-" + id); //not found song message
        }
        return Song;
    }
}
