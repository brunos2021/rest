package com.brunos.rest.AudioDB.controllers;

import com.brunos.rest.AudioDB.DTO.ArtistDTO;
import com.brunos.rest.AudioDB.DTO.ArtistDTOservice;
import com.brunos.rest.AudioDB.model.Artist;
import com.brunos.rest.AudioDB.model.ArtistsAudioDB;
import com.brunos.rest.AudioDB.model.Song;
import com.brunos.rest.AudioDB.services.ArtistService;
import com.brunos.rest.AudioDB.services.AudiodbService;
import com.brunos.rest.AudioDB.services.SongService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/artists")
public class ArtistController {

    @Autowired
    private ArtistService Service;
    @Autowired
    private AudiodbService AudioDBservice;
    @Autowired
    private ArtistDTOservice artistDTOservice;


    @Autowired
    private SongService ServiceOfSongs;
    public ArtistController(SongService serviceOfSongs) {
        ServiceOfSongs = serviceOfSongs;
    }

    // GET /artists
    @GetMapping("")
    public List<Artist> getAllArtists() {
        return Service.getAll();
    }

    // GET /artists/{id}
    @GetMapping("/{id}")
    public Artist getArtist(@PathVariable("id") int id) throws NotFoundException {
        Artist Artist = Service.getArtistById(id);
        if (Artist==null){
            throw new NotFoundException("not found artist with id-" + id);// no found artist message
        }
        return Artist;
    }

//    @GetMapping("/api/{name}")
//    public ArtistsAudioDB getArtistFromAudioDB(@PathVariable("name") String name) {
//        ArtistsAudioDB artists = AudioDBservice.getArtistInfoByName(name);
//        return artists;
//    }
    // Get /artists/{name}
    @GetMapping("/api/{name}")
    public ArtistDTO test(@PathVariable("name")String name){
        ArtistsAudioDB artistsAudioDB = AudioDBservice.getArtistInfoByName(name);
        ArtistDTO artistDTO = artistDTOservice.getArtistInfoFromArtistAudioDBClass(artistsAudioDB);
        return artistDTO;
    }


    //GET /artists/{id}/songs
    @GetMapping("/{id}/songs")
    public List<Song> getSongsByArtist (@PathVariable("id") int id) throws NotFoundException {
        List<Song> TheListOfSongs = ServiceOfSongs.getSongByArtistID(id);
        System.out.println(TheListOfSongs);

        return TheListOfSongs;
    }
}
