package com.brunos.rest.AudioDB.model;


public class Artist {
    private Integer id;
    private String name;
    private String surname;
    private String picture;
    private Integer bornyear;
    private Integer gender;
    private String bio;

    public Artist(Integer id, String name, String surname, String picture, Integer bornyear, Integer gender, String bio) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.picture = picture;
        this.bornyear = bornyear;
        this.gender = gender;
        this.bio = bio;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", bornyear=" + bornyear +
                ", picture='" + picture + '\'' +
                ", gender=" + gender +
                ", bio='" + bio + '\'' +
                "}";

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getBornyear() {
        return bornyear;
    }

    public void setBornyear(Integer bornyear) {
        this.bornyear = bornyear;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
