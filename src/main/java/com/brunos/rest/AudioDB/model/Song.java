package com.brunos.rest.AudioDB.model;



public class Song {
    private Integer id;
    private String name;
    private String url;
    private String releaseDate;
    private Integer artist_id;
    private String artistName;

    public Song(Integer id, String name, String url, String releaseDate, Integer artist_id, String artistName) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.releaseDate = releaseDate;
        this.artist_id = artist_id;
        this.artistName = artistName;
    }

    @Override
    public String toString(){
        return "Song{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", artist_id=" + artist_id +
                ", artistName='" + artistName + '\'' +
                "}";

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getArtist_id() {
        return artist_id;
    }

    public void setArtist_id(Integer artist_id) {
        this.artist_id = artist_id;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}


