package com.brunos.rest.AudioDB.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ArtistsAudioDB {
    @JsonProperty("artists")
    private List<ArtistAudioDB> artists;

    public List<ArtistAudioDB> getArtists() {
        return artists;
    }

    public void setArtists(List<ArtistAudioDB> artists) {
        this.artists = artists;
    }
}
