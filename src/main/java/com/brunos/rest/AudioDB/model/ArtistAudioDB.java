package com.brunos.rest.AudioDB.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString), Root.class); */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ArtistAudioDB{
        @JsonProperty("idArtist")
        public String getIdArtist() {
                return this.idArtist; }
        public void setIdArtist(String idArtist) {
                this.idArtist = idArtist; }
        String idArtist;
        @JsonProperty("strArtist")
        public String getStrArtist() {
                return this.strArtist; }
        public void setStrArtist(String strArtist) {
                this.strArtist = strArtist; }
        String strArtist;
        @JsonProperty("strArtistStripped")
        public Object getStrArtistStripped() {
                return this.strArtistStripped; }
        public void setStrArtistStripped(Object strArtistStripped) {
                this.strArtistStripped = strArtistStripped; }
        Object strArtistStripped;
        @JsonProperty("strArtistAlternate")
        public String getStrArtistAlternate() {
                return this.strArtistAlternate; }
        public void setStrArtistAlternate(String strArtistAlternate) {
                this.strArtistAlternate = strArtistAlternate; }
        String strArtistAlternate;
        @JsonProperty("strLabel")
        public String getStrLabel() {
                return this.strLabel; }
        public void setStrLabel(String strLabel) {
                this.strLabel = strLabel; }
        String strLabel;
        @JsonProperty("idLabel")
        public String getIdLabel() {
                return this.idLabel; }
        public void setIdLabel(String idLabel) {
                this.idLabel = idLabel; }
        String idLabel;
        @JsonProperty("intFormedYear")
        public String getIntFormedYear() {
                return this.intFormedYear; }
        public void setIntFormedYear(String intFormedYear) {
                this.intFormedYear = intFormedYear; }
        String intFormedYear;
        @JsonProperty("intBornYear")
        public String getIntBornYear() {
                return this.intBornYear; }
        public void setIntBornYear(String intBornYear) {
                this.intBornYear = intBornYear; }
        String intBornYear;
        @JsonProperty("intDiedYear")
        public Object getIntDiedYear() {
                return this.intDiedYear; }
        public void setIntDiedYear(Object intDiedYear) {
                this.intDiedYear = intDiedYear; }
        Object intDiedYear;
        @JsonProperty("strDisbanded")
        public Object getStrDisbanded() {
                return this.strDisbanded; }
        public void setStrDisbanded(Object strDisbanded) {
                this.strDisbanded = strDisbanded; }
        Object strDisbanded;
        @JsonProperty("strStyle")
        public String getStrStyle() {
                return this.strStyle; }
        public void setStrStyle(String strStyle) {
                this.strStyle = strStyle; }
        String strStyle;
        @JsonProperty("strGenre")
        public String getStrGenre() {
                return this.strGenre; }
        public void setStrGenre(String strGenre) {
                this.strGenre = strGenre; }
        String strGenre;
        @JsonProperty("strMood")
        public String getStrMood() {
                return this.strMood; }
        public void setStrMood(String strMood) {
                this.strMood = strMood; }
        String strMood;
        @JsonProperty("strWebsite")
        public String getStrWebsite() {
                return this.strWebsite; }
        public void setStrWebsite(String strWebsite) {
                this.strWebsite = strWebsite; }
        String strWebsite;
        @JsonProperty("strFacebook")
        public String getStrFacebook() {
                return this.strFacebook; }
        public void setStrFacebook(String strFacebook) {
                this.strFacebook = strFacebook; }
        String strFacebook;
        @JsonProperty("strTwitter")
        public String getStrTwitter() {
                return this.strTwitter; }
        public void setStrTwitter(String strTwitter) {
                this.strTwitter = strTwitter; }
        String strTwitter;
        @JsonProperty("strBiographyEN")
        public String getStrBiographyEN() {
                return this.strBiographyEN; }
        public void setStrBiographyEN(String strBiographyEN) {
                this.strBiographyEN = strBiographyEN; }
        String strBiographyEN;
        @JsonProperty("strBiographyDE")
        public String getStrBiographyDE() {
                return this.strBiographyDE; }
        public void setStrBiographyDE(String strBiographyDE) {
                this.strBiographyDE = strBiographyDE; }
        String strBiographyDE;
        @JsonProperty("strBiographyFR")
        public String getStrBiographyFR() {
                return this.strBiographyFR; }
        public void setStrBiographyFR(String strBiographyFR) {
                this.strBiographyFR = strBiographyFR; }
        String strBiographyFR;
        @JsonProperty("strBiographyCN")
        public String getStrBiographyCN() {
                return this.strBiographyCN; }
        public void setStrBiographyCN(String strBiographyCN) {
                this.strBiographyCN = strBiographyCN; }
        String strBiographyCN;
        @JsonProperty("strBiographyIT")
        public String getStrBiographyIT() {
                return this.strBiographyIT; }
        public void setStrBiographyIT(String strBiographyIT) {
                this.strBiographyIT = strBiographyIT; }
        String strBiographyIT;
        @JsonProperty("strBiographyJP")
        public String getStrBiographyJP() {
                return this.strBiographyJP; }
        public void setStrBiographyJP(String strBiographyJP) {
                this.strBiographyJP = strBiographyJP; }
        String strBiographyJP;
        @JsonProperty("strBiographyRU")
        public String getStrBiographyRU() {
                return this.strBiographyRU; }
        public void setStrBiographyRU(String strBiographyRU) {
                this.strBiographyRU = strBiographyRU; }
        String strBiographyRU;
        @JsonProperty("strBiographyES")
        public String getStrBiographyES() {
                return this.strBiographyES; }
        public void setStrBiographyES(String strBiographyES) {
                this.strBiographyES = strBiographyES; }
        String strBiographyES;
        @JsonProperty("strBiographyPT")
        public String getStrBiographyPT() {
                return this.strBiographyPT; }
        public void setStrBiographyPT(String strBiographyPT) {
                this.strBiographyPT = strBiographyPT; }
        String strBiographyPT;
        @JsonProperty("strBiographySE")
        public String getStrBiographySE() {
                return this.strBiographySE; }
        public void setStrBiographySE(String strBiographySE) {
                this.strBiographySE = strBiographySE; }
        String strBiographySE;
        @JsonProperty("strBiographyNL")
        public String getStrBiographyNL() {
                return this.strBiographyNL; }
        public void setStrBiographyNL(String strBiographyNL) {
                this.strBiographyNL = strBiographyNL; }
        String strBiographyNL;
        @JsonProperty("strBiographyHU")
        public String getStrBiographyHU() {
                return this.strBiographyHU; }
        public void setStrBiographyHU(String strBiographyHU) {
                this.strBiographyHU = strBiographyHU; }
        String strBiographyHU;
        @JsonProperty("strBiographyNO")
        public String getStrBiographyNO() {
                return this.strBiographyNO; }
        public void setStrBiographyNO(String strBiographyNO) {
                this.strBiographyNO = strBiographyNO; }
        String strBiographyNO;
        @JsonProperty("strBiographyIL")
        public String getStrBiographyIL() {
                return this.strBiographyIL; }
        public void setStrBiographyIL(String strBiographyIL) {
                this.strBiographyIL = strBiographyIL; }
        String strBiographyIL;
        @JsonProperty("strBiographyPL")
        public String getStrBiographyPL() {
                return this.strBiographyPL; }
        public void setStrBiographyPL(String strBiographyPL) {
                this.strBiographyPL = strBiographyPL; }
        String strBiographyPL;
        @JsonProperty("strGender")
        public String getStrGender() {
                return this.strGender; }
        public void setStrGender(String strGender) {
                this.strGender = strGender; }
        String strGender;
        @JsonProperty("intMembers")
        public String getIntMembers() {
                return this.intMembers; }
        public void setIntMembers(String intMembers) {
                this.intMembers = intMembers; }
        String intMembers;
        @JsonProperty("strCountry")
        public String getStrCountry() {
                return this.strCountry; }
        public void setStrCountry(String strCountry) {
                this.strCountry = strCountry; }
        String strCountry;
        @JsonProperty("strCountryCode")
        public String getStrCountryCode() {
                return this.strCountryCode; }
        public void setStrCountryCode(String strCountryCode) {
                this.strCountryCode = strCountryCode; }
        String strCountryCode;
        @JsonProperty("strArtistThumb")
        public String getStrArtistThumb() {
                return this.strArtistThumb; }
        public void setStrArtistThumb(String strArtistThumb) {
                this.strArtistThumb = strArtistThumb; }
        String strArtistThumb;
        @JsonProperty("strArtistLogo")
        public String getStrArtistLogo() {
                return this.strArtistLogo; }
        public void setStrArtistLogo(String strArtistLogo) {
                this.strArtistLogo = strArtistLogo; }
        String strArtistLogo;
        @JsonProperty("strArtistClearart")
        public String getStrArtistClearart() {
                return this.strArtistClearart; }
        public void setStrArtistClearart(String strArtistClearart) {
                this.strArtistClearart = strArtistClearart; }
        String strArtistClearart;
        @JsonProperty("strArtistWideThumb")
        public String getStrArtistWideThumb() {
                return this.strArtistWideThumb; }
        public void setStrArtistWideThumb(String strArtistWideThumb) {
                this.strArtistWideThumb = strArtistWideThumb; }
        String strArtistWideThumb;
        @JsonProperty("strArtistFanart")
        public String getStrArtistFanart() {
                return this.strArtistFanart; }
        public void setStrArtistFanart(String strArtistFanart) {
                this.strArtistFanart = strArtistFanart; }
        String strArtistFanart;
        @JsonProperty("strArtistFanart2")
        public String getStrArtistFanart2() {
                return this.strArtistFanart2; }
        public void setStrArtistFanart2(String strArtistFanart2) {
                this.strArtistFanart2 = strArtistFanart2; }
        String strArtistFanart2;
        @JsonProperty("strArtistFanart3")
        public String getStrArtistFanart3() {
                return this.strArtistFanart3; }
        public void setStrArtistFanart3(String strArtistFanart3) {
                this.strArtistFanart3 = strArtistFanart3; }
        String strArtistFanart3;
        @JsonProperty("strArtistFanart4")
        public String getStrArtistFanart4() {
                return this.strArtistFanart4; }
        public void setStrArtistFanart4(String strArtistFanart4) {
                this.strArtistFanart4 = strArtistFanart4; }
        String strArtistFanart4;
        @JsonProperty("strArtistBanner")
        public String getStrArtistBanner() {
                return this.strArtistBanner; }
        public void setStrArtistBanner(String strArtistBanner) {
                this.strArtistBanner = strArtistBanner; }
        String strArtistBanner;
        @JsonProperty("strMusicBrainzID")
        public String getStrMusicBrainzID() {
                return this.strMusicBrainzID; }
        public void setStrMusicBrainzID(String strMusicBrainzID) {
                this.strMusicBrainzID = strMusicBrainzID; }
        String strMusicBrainzID;
        @JsonProperty("strISNIcode")
        public Object getStrISNIcode() {
                return this.strISNIcode; }
        public void setStrISNIcode(Object strISNIcode) {
                this.strISNIcode = strISNIcode; }
        Object strISNIcode;
        @JsonProperty("strLastFMChart")
        public String getStrLastFMChart() {
                return this.strLastFMChart; }
        public void setStrLastFMChart(String strLastFMChart) {
                this.strLastFMChart = strLastFMChart; }
        String strLastFMChart;
        @JsonProperty("intCharted")
        public String getIntCharted() {
                return this.intCharted; }
        public void setIntCharted(String intCharted) {
                this.intCharted = intCharted; }
        String intCharted;
        @JsonProperty("strLocked")
        public String getStrLocked() {
                return this.strLocked; }
        public void setStrLocked(String strLocked) {
                this.strLocked = strLocked; }
        String strLocked;

//        public ArtistAudioDB(String idArtist, String strArtist, Object strArtistStripped, String strArtistAlternate, String strLabel, String idLabel, String intFormedYear, String intBornYear, Object intDiedYear, Object strDisbanded, String strStyle, String strGenre, String strMood, String strWebsite, String strFacebook, String strTwitter, String strBiographyEN, String strBiographyDE, String strBiographyFR, String strBiographyCN, String strBiographyIT, String strBiographyJP, String strBiographyRU, String strBiographyES, String strBiographyPT, String strBiographySE, String strBiographyNL, String strBiographyHU, String strBiographyNO, String strBiographyIL, String strBiographyPL, String strGender, String intMembers, String strCountry, String strCountryCode, String strArtistThumb, String strArtistLogo, String strArtistClearart, String strArtistWideThumb, String strArtistFanart, String strArtistFanart2, String strArtistFanart3, String strArtistFanart4, String strArtistBanner, String strMusicBrainzID, Object strISNIcode, String strLastFMChart, String intCharted, String strLocked) {
//                this.idArtist = idArtist;
//                this.strArtist = strArtist;
//                this.strArtistStripped = strArtistStripped;
//                this.strArtistAlternate = strArtistAlternate;
//                this.strLabel = strLabel;
//                this.idLabel = idLabel;
//                this.intFormedYear = intFormedYear;
//                this.intBornYear = intBornYear;
//                this.intDiedYear = intDiedYear;
//                this.strDisbanded = strDisbanded;
//                this.strStyle = strStyle;
//                this.strGenre = strGenre;
//                this.strMood = strMood;
//                this.strWebsite = strWebsite;
//                this.strFacebook = strFacebook;
//                this.strTwitter = strTwitter;
//                this.strBiographyEN = strBiographyEN;
//                this.strBiographyDE = strBiographyDE;
//                this.strBiographyFR = strBiographyFR;
//                this.strBiographyCN = strBiographyCN;
//                this.strBiographyIT = strBiographyIT;
//                this.strBiographyJP = strBiographyJP;
//                this.strBiographyRU = strBiographyRU;
//                this.strBiographyES = strBiographyES;
//                this.strBiographyPT = strBiographyPT;
//                this.strBiographySE = strBiographySE;
//                this.strBiographyNL = strBiographyNL;
//                this.strBiographyHU = strBiographyHU;
//                this.strBiographyNO = strBiographyNO;
//                this.strBiographyIL = strBiographyIL;
//                this.strBiographyPL = strBiographyPL;
//                this.strGender = strGender;
//                this.intMembers = intMembers;
//                this.strCountry = strCountry;
//                this.strCountryCode = strCountryCode;
//                this.strArtistThumb = strArtistThumb;
//                this.strArtistLogo = strArtistLogo;
//                this.strArtistClearart = strArtistClearart;
//                this.strArtistWideThumb = strArtistWideThumb;
//                this.strArtistFanart = strArtistFanart;
//                this.strArtistFanart2 = strArtistFanart2;
//                this.strArtistFanart3 = strArtistFanart3;
//                this.strArtistFanart4 = strArtistFanart4;
//                this.strArtistBanner = strArtistBanner;
//                this.strMusicBrainzID = strMusicBrainzID;
//                this.strISNIcode = strISNIcode;
//                this.strLastFMChart = strLastFMChart;
//                this.intCharted = intCharted;
//                this.strLocked = strLocked;
//        }
}

