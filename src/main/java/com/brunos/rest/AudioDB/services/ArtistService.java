package com.brunos.rest.AudioDB.services;

import com.brunos.rest.AudioDB.model.Artist;
import com.brunos.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ArtistService {
    private static List<Artist> Artists = new ArrayList<>();
    @Autowired
    private AudiodbService service;

    static {
        Artists.add(
                new Artist(
                        1,
                        "Name 1",
                        "Surname 1",
                        "",
                        2000,
                        1,
                        "artist1...."

                )
        );
        Artists.add(
                new Artist(
                        2,
                        "Name 2",
                        "Surname 2",
                        "",
                        1999,
                        1,
                        "artist2...."

                )
        );
        Artists.add(
                new Artist(
                        3,
                        "Name 3",
                        "Surname 3",
                        "",
                        1950,
                        0,
                        "artist3...."

                )
        );
    }


    public List<Artist> getAll() {
            return Artists;
        }
        public Artist getArtistById(int id) {
            for (Artist Artist : Artists) {
                if (Artist.getId()==id) {
                    return Artist;
                }
            }

            return null;
        }

        public Artist getArtistByName(String name) throws NotFoundException {
        throw new UnsupportedOperationException();
//        Artist artist = service.getArtistInfoByName(name);
//            if(artist==null){
//                throw new NotFoundException("No artist with name-" + name); //no songs of this artist message
//            }else return artist;

        }

    }

