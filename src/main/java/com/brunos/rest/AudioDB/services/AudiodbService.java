package com.brunos.rest.AudioDB.services;

import com.brunos.rest.AudioDB.AudioDbClient;
import com.brunos.rest.AudioDB.model.ArtistsAudioDB;
import com.brunos.rest.AudioDB.model.Song;
import com.brunos.rest.exception.NotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AudiodbService {
    private AudioDbClient audioDbClient;

    @Autowired
    public AudiodbService(AudioDbClient audioDbClient) {
        this.audioDbClient = audioDbClient;
    }

    public List<Song> getSongsByArtistID(int id) {
        throw new UnsupportedOperationException("");
    }


    public ArtistsAudioDB getArtistInfoByName(String name) {
        ObjectMapper objectMapper = new ObjectMapper();
        String artistInfoByName = audioDbClient.getArtistInfoByNameRequest(name);
        try {
            return objectMapper.readValue(artistInfoByName, ArtistsAudioDB.class);
        } catch (JsonProcessingException e) {
            throw new NotFoundException(e.getMessage());
        }
    }

}
