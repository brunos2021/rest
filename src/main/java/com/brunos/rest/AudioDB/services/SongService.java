package com.brunos.rest.AudioDB.services;

import com.brunos.rest.AudioDB.model.Song;
import com.brunos.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SongService {
    private static List<Song> Songs = new ArrayList<>();

    static {
            Songs.add(
                    new Song(
                             1,
                            "Song 1",
                            "https://www.youtube.com/watch?v=X-yIEMduRXk",
                            "10/10/2010",
                            1,
                            "Name 1"


                    )
            );
        Songs.add(
                new Song(
                        2,
                        "Song 2",
                        "https://www.youtube.com/watch?v=tollGa3S0o8",
                        "10/10/2011",
                        2,
                        "Name 2"


                )
        );
        Songs.add(
                new Song(
                        3,
                        "Song 3",
                        "https://www.youtube.com/watch?v=_dlExeOyFh4",
                        "19/10/2015",
                        1,
                        "Name 1"


                )
        );
        }

    private AudiodbService service;
    @Autowired
    public SongService(AudiodbService service){
        this.service=service;
    }

    public List<Song> getAll() { // return all songs
        return Songs;
    }
    public Song getSongByID(int id) { // return song by id
        for (Song Song : Songs) {
            if (Song.getId()==id) {
                return Song;
            }
        }

        return null;
    }

    public List<Song> getSongByArtistID(int id) throws NotFoundException { // return songs by artist id
        List<Song> TheListOfSongs = new ArrayList<>();
        for(Song Song : Songs){
            if(Song.getArtist_id()==id){

                TheListOfSongs.add(Song);
            }


        /*List<Song> TheListOfSongs = service.getSongsByArtistID(id);

        if (TheListOfSongs.isEmpty()) {
            throw new NotFoundException("No songs from artist with id-" + id); //no songs of this artist message
        }*/

        }
        return TheListOfSongs;
    }
}
