package com.brunos.rest.AudioDB;

import com.brunos.rest.exception.NotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class AudioDbClient {

    public String getArtistInfoByNameRequest(String name)
    {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://www.theaudiodb.com/api/v1/json/2/search.php?s="+name))
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (IOException | InterruptedException e) {
            throw new NotFoundException(e.getMessage());
        }
    }
}
