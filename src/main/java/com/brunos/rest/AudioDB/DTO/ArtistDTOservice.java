package com.brunos.rest.AudioDB.DTO;

import com.brunos.rest.AudioDB.model.ArtistsAudioDB;
import org.springframework.stereotype.Service;

@Service
public class ArtistDTOservice {
    ArtistDTO artistDTO = new ArtistDTO();

    public ArtistDTO getArtistInfoFromArtistAudioDBClass(ArtistsAudioDB artistsAudioDB) {
        artistDTO.setArtistname(artistsAudioDB.getArtists().get(0).getStrArtist());
        artistDTO.setImage(artistsAudioDB.getArtists().get(0).getStrArtistThumb());
        artistDTO.setBio(artistsAudioDB.getArtists().get(0).getStrBiographyEN());
        artistDTO.setFacebook(artistsAudioDB.getArtists().get(0).getStrFacebook());
        artistDTO.setTwitter(artistsAudioDB.getArtists().get(0).getStrTwitter());
        return artistDTO;
    }
}
