package com.brunos.rest.user;

import com.sun.istack.NotNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

public class User {

    private Integer id;

    @Min(value = 3, message = "Name is too short.")
    private String name;

    @Min(value = 3, message = "Surname is too short.")
    private String surname;

    @Past(message = "Date of birth must be in the past.")
    private Date birthday;
    private String picture;

    @NotNull
    private Integer gender;

    @Email(message = "Email address is wrong.")
    private String email;

    @Size(min = 10, max = 10, message = "Mobile number is incorrect. Must be 10 digits")
    private String mobile;

    private String facebook_url;
    private String instagram_url;
    private String twitter_url;

    @NotNull
    private Date register_date;

    public User(Integer id, String name, String surname, Date birthday, String picture, Integer gender, String email, String mobile, String facebook_url, String instagram_url, String twitter_url, Date register_date) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.picture = picture;
        this.gender = gender;
        this.email = email;
        this.mobile = mobile;
        this.facebook_url = facebook_url;
        this.instagram_url = instagram_url;
        this.twitter_url = twitter_url;
        this.register_date = register_date;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthday=" + birthday +
                ", picture='" + picture + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", mobile=" + mobile +
                ", facebook_url='" + facebook_url + '\'' +
                ", instagram_url='" + instagram_url + '\'' +
                ", twitter_url='" + twitter_url + '\'' +
                ", register_date=" + register_date +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFacebook_url() {
        return facebook_url;
    }

    public void setFacebook_url(String facebook_url) {
        this.facebook_url = facebook_url;
    }

    public String getInstagram_url() {
        return instagram_url;
    }

    public void setInstagram_url(String instagram_url) {
        this.instagram_url = instagram_url;
    }

    public String getTwitter_url() {
        return twitter_url;
    }

    public void setTwitter_url(String twitter_url) {
        this.twitter_url = twitter_url;
    }

    public Date getRegister_date() {
        return register_date;
    }

    public void setRegister_date(Date register_date) {
        this.register_date = register_date;
    }
}
