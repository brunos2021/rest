package com.brunos.rest.user;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class UserService {
    private static List<User> users = new ArrayList<>();
    private static int usersCount = 3;

    static {
        users.add(
            new User(
                1,
                "Name 1",
                "Surname 1",
                new Date(),
                "",
                1,
                "user1@gmail.com",
                "1234567890",
                "https://facebook.com",
                "https://instagram.com",
                "https://twitter.com",
                new Date()
            )
        );

        users.add(
                new User(
                        2,
                        "Name 2",
                        "Surname 2",
                        new Date(),
                        "",
                        1,
                        "user2@gmail.com",
                        "1234567890",
                        "https://facebook.com",
                        "https://instagram.com",
                        "https://twitter.com",
                        new Date()
                )
        );

        users.add(
                new User(
                        3,
                        "Name 3",
                        "Surname 3",
                        new Date(),
                        "",
                        1,
                        "user3@gmail.com",
                        "1234567890",
                        "https://facebook.com",
                        "https://instagram.com",
                        "https://twitter.com",
                        new Date()
                )
        );
    }

    public List<User> getAll() {
        return users;
    }

    public User getUserById(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }

        return null;
    }

    public User saveUser(User user) {
        if (user.getId() == null) {
            user.setId(++usersCount);
        }
        users.add(user);
        return user;
    }
}
