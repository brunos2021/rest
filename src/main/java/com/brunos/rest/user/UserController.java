package com.brunos.rest.user;

import com.brunos.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService service;

    // GET /users
    @GetMapping("")
    public List<User> getAllUsers() {

        return service.getAll();
    }

    // GET /users/{id}
    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") int id) {

        User user = service.getUserById(id);
        if (user == null) {
            throw new NotFoundException("id-" + id);
        }

        return user;
    }

    // POST /users
    @PostMapping()
    public ResponseEntity<Object> createUser(@Validated @RequestBody User user) {
        User savedUser = service.saveUser(user);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedUser.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }
}
