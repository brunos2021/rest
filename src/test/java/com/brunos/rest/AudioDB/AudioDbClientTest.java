package com.brunos.rest.AudioDB;

import com.brunos.rest.exception.NotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


import static org.junit.jupiter.api.Assertions.*;

class AudioDbClientTest {

    private AudioDbClient request = new AudioDbClient();

    @Test
    void getArtistInfoByNameRequestMustReturnAString() {
        String ServerRequest = request.getArtistInfoByNameRequest("coldplay");
        assertNotNull(ServerRequest);
    }

}