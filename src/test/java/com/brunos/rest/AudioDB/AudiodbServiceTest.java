package com.brunos.rest.AudioDB;

import com.brunos.rest.AudioDB.model.ArtistAudioDB;
import com.brunos.rest.AudioDB.model.ArtistsAudioDB;
import com.brunos.rest.AudioDB.services.AudiodbService;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

//@AutoConfigureMockMvc

class AudiodbServiceTest {
    private AudioDbClient mock;
    private AudiodbService service;

    @BeforeEach
    public void setUp()
    {
        mock = Mockito.mock(AudioDbClient.class);
        service = new AudiodbService(mock);
    }


    @Test
    void getArtistInfoFromAudioDBByNameReturnTheCorrectArtist() throws Exception{
//        ArtistAudioDB Artists = null;
//        //Artists.getArtistByName("hasdgjaw");
//        when(mock.getArtistByName(anyString()))
//                .thenThrow(new NotFoundException("No artist with name-"));
//        fail("NotFoundException should have been returned");

        try(InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("coldplay.json")) {
            String contents = IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
            when(mock.getArtistInfoByNameRequest("coldplay")).thenReturn(contents);
            ArtistsAudioDB artistsAudioDB = service.getArtistInfoByName("coldplay");

            assertEquals(1,artistsAudioDB.getArtists().size());
            //ArtistAudioDB artistAudioDB = artistsAudioDB.getArtists().get(0);

            ArtistAudioDB artistAudioDB = artistsAudioDB.getArtists().get(0);
            assertEquals("111239",artistAudioDB.getIdArtist());
        }
    }

    @Test
    void getArtistInfoFromAudioDBbyNameNotExistingShouldThrowNotFoundException(){

    }
}