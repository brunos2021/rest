package com.brunos.rest.AudioDB;

import com.brunos.rest.AudioDB.model.Artist;
import com.brunos.rest.AudioDB.services.ArtistService;
import com.brunos.rest.RestApplication;
import com.brunos.rest.exception.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes= RestApplication.class)
public class ArtistServiceTest {

    private ArtistService service = new ArtistService();

    @Test
    public void getArtistByIdWhenIdIsExisting() {
        Artist Artist = service.getArtistById(1);
        assertEquals(1,Artist.getId());

    }
    @Test
    public void getArtistByIdWhenIdIsNotExisting() {
        try {
            Artist Artist = service.getArtistById(0);

        }catch (NotFoundException e){
            fail("NoFoundException should have beed returned");
        }
    }


    @Test
    public void getAllArtistMustReturnAtLeastOne(){
        List<Artist> Artist = service.getAll();
        assertNotEquals(0,Artist.size());
    }
}