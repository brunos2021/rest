package com.brunos.rest.AudioDB;

import com.brunos.rest.AudioDB.model.Song;
import com.brunos.rest.AudioDB.services.AudiodbService;
import com.brunos.rest.AudioDB.services.SongService;
import com.brunos.rest.RestApplication;
import com.brunos.rest.exception.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes=RestApplication.class)
@AutoConfigureMockMvc
public class SongServiceTest {
    private AudiodbService mock;
    private SongService service;

    @BeforeEach
    public void setUp()
    {
         mock = Mockito.mock(AudiodbService.class);
         service = new SongService(mock);
    }

    @Test
    public void getSongByArtistIDWhenAtLEastOneSongIsReturned() throws Exception {

//        Song song = new Song(1, "onoma", "hy", "2323", 2, "#3");
//        when(mock.getSongsByArtistID(anyInt()))
//                .thenReturn(Arrays.asList(song));
        Song song = service.getSongByID(2);
        List<Song> songByArtistID = service.getSongByArtistID(2);
        assertEquals(1, songByArtistID.size());
        assertEquals(song, songByArtistID.get(0));
}
    @Test
    public void getSongByArtistWhenNoSongsExist() {
        when(mock.getSongsByArtistID(anyInt())).thenReturn(new ArrayList<>());

        try {
            List<Song> songs = service.getSongByArtistID(3);
        } catch (NotFoundException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail("NotFoundException should have been returned");
        }
    }

    @Test
    public void getSongByIdWhenSongIsExisting(){
        Song song = service.getSongByID(1);
        assertEquals(1,song.getId());
    }
    @Test
    public void getSongByIdWhenSongIsNotExisting(){
        try{
            Song song = service.getSongByID(0);
        }catch(NotFoundException e){
            fail("NoFoundException should have beed returned");
        }



    }
    @Test
    public void getAllSongsMustReturnAtLeastOne(){
        List<Song> Songs = service.getAll();
        assertNotEquals(0,Songs.size());
    }
}