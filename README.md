# Getting Started

PLEASE read **ALL** the README **BEFORE** you begin anything!

### Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.6/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.6/maven-plugin/reference/html/#build-image)
* [Spring Web Services](https://docs.spring.io/spring-boot/docs/2.5.6/reference/htmlsingle/#boot-features-webservices)

## What you will need
1. Installed Maven. Be sure that it runs on your pc.
   * More info: https://maven.apache.org/install.html
   > Install maven by clicking on maven tab right on intellij IDEA section. select **Lifecycle** -> **install**
2. Installed and running Docker
   * More info: https://www.docker.com


## Where to begin

1. Write this command on your terminal
    ####Linux based
    > ./mvnw package && java -jar target/gs-spring-boot-docker-0.1.0.jar

    #### Windows
    > ./mvnw package ; java -jar target/rest-0.0.1-SNAPSHOT.jar

2. Containerize the docker image
    > docker build -t brunos/rest-service .
 
3. Build docker image
    > ./mvnw spring-boot:build-image -D spring-boot.build-image.imageName=brunos/rest-service

4. Start the container: 
    > docker run -p 8080:8080 -t brunos/rest-service

5. Go to: http://localhost:8080/greeting?name=Tester
6. You must see something like this: 
    > {"id":2,"content":"Hello, Tester!"}


## NOTES
* You **MUST** complete all the above steps in order to run the project
* **EVERYTIME** you change something on your code and you want to see the results you **MUST FIRST** run the command **3** and then **4**. Try to change some code on the project and see if you get any new results.
* If you don't run the commands in **3** and **4** nothing will be different even if you saved your code.
* In order to understand how Docker works PLEASE before you begin run this command on your terminal:
    > docker run -p 8080:8080 -t brunos2021/rest-service
    * After that command you can run the published project. The one that will be running on one computer in order to be shown to the client (professor)
    * ONE person will build this after all commits are done and a day before the presentation
    * This docker is the one that everybody can download and run it on his PC

## API
### USERS

> GET /users

Get all users that are registered


> GET /users/{id}

Get info of a specific user

> POST /users

Create user

### ARTISTS

> GET /artists/{id}

Get artist from id

>GET /artists/api/{name}

Get artist from name from AudioDB

>GET /artists/{id}/songs

Get all songs from artist id

### SONGS

to run the those rest services just type this command on your cmd (docker): docker run -p 8080:8080 -t brunos2021/rest-service